package vs.layour.dadm_u1_proyectosge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import vs.layour.dadm_u1_proyectosge.databinding.ActivityMisDatosBinding
import vs.layour.dadm_u1_proyectosge.models.Alumno

class MisDatosActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMisDatosBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMisDatosBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //  var bdString = resources.getString(R.string.baseDeDatos)
        // val mibd = Gson().fromJson(bdString, dataBase::class.java)
        //  val alumno = mibd.alumnos[0]


        val alumno = intent.extras?.getSerializable("ALUMNO")as Alumno
        //println(alumno)

        binding.textViewMisDatosNombre.text= alumno.nombre
        binding.txtViewCurp.text = alumno.curp
        binding.txtfNacimiento.text = alumno.fNacimiento
        binding.txtsexo.text = alumno.sexo
        binding.txtViewMisDatosNControl.text =alumno.noControl
        binding.txtViewSemestre.text= alumno.semestre
        binding.txtViewCarrera.text = alumno.carrera
        binding.txtcorreopersonal.text = alumno.correoPersonal
        binding.txtcorreoinstitucional.text = alumno.correoInstitucional







    }
}