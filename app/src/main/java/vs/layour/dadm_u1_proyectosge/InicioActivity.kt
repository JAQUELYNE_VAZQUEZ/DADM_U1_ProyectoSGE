package vs.layour.dadm_u1_proyectosge

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import vs.layour.dadm_u1_proyectosge.databinding.ActivityInicioBinding
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.dataBase


class InicioActivity : AppCompatActivity() {

    private lateinit var binding: ActivityInicioBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInicioBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var btnAcceder  = findViewById<Button>(R.id.btnInicio_acceder)

        val bd = intent.extras?.getSerializable("BD") as dataBase
        val alumno = intent.extras?.getSerializable("ALUMNO") as Alumno

        for (i in AccederActivity.bd.evaluacionesDocentes){
            println(i)
        }

        btnAcceder.setOnClickListener{
            val intent = Intent (this, MenuAlumnoActivity::class.java)
            intent.putExtra("ALUMNO", alumno)
            intent.putExtra("BD", bd)
            startActivity(intent)
        }
    }
}