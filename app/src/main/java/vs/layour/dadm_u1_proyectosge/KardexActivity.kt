package vs.layour.dadm_u1_proyectosge

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

import vs.layour.dadm_u1_proyectosge.databinding.ActivityKardexBinding

class KardexActivity : AppCompatActivity() {


    private lateinit var binding: ActivityKardexBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKardexBinding.inflate(layoutInflater)
        setContentView(binding.root)


    }
}