package vs.layour.dadm_u1_proyectosge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Adapter
import android.widget.LinearLayout
import com.google.gson.Gson
import vs.layour.dadm_u1_proyectosge.adapters.*
import vs.layour.dadm_u1_proyectosge.databinding.ActivityHorarioBinding
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.dataBase

class HorarioActivity : AppCompatActivity() {


  /*  lateinit var linearLunes: LinearLayout
    lateinit var linearMartes: LinearLayout
    lateinit var linearMiercoles: LinearLayout
    lateinit var linearJueves: LinearLayout
    lateinit var linearViernes: LinearLayout
*/
    private lateinit var binding: ActivityHorarioBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHorarioBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val alumno = intent.extras?.getSerializable("ALUMNO")as Alumno

        val bdString = resources.getString(R.string.baseDeDatos)
        val bdJson= Gson().fromJson(bdString, dataBase::class.java)
        val horario = bdJson.horario

        val lunes = horario[0].lunes
        val lvlunes = binding.lvLunes
        lvlunes.adapter = LunesHorarioAdapter(this, R.layout.horario, lunes)



        val martes = horario[1].martes
        val lvmartes = binding.lvMartes
        lvmartes.adapter = MartesHorarioAdapter(this, R.layout.horario, martes)

        val miercoles = horario[2].miercoles
        val lvmiercoles = binding.lvMiercoles
        lvmiercoles.adapter = MiercolesHorarioAdapter(this, R.layout.horario, miercoles)

        val jueves = horario[3].jueves
        val lvjueves = binding.lvJueves
        lvjueves.adapter = JuevesHorarioAdapter(this, R.layout.horario,jueves)

        val viernes = horario[4].viernes
        val lvviernes = binding.lvViernes
        lvviernes.adapter = ViernesHorarioAdapter(this, R.layout.horario, viernes)





       // println(horario[0].lunes)

/*
        linearLunes = binding.linearHorarioLunes
        linearMartes = binding.linearHorarioMartes
        linearMiercoles = binding.linearHorarioMiercoles
        linearJueves = binding.linearHorarioJueves
        linearViernes = binding.linearHorarioViernes


        var stringBD = intent.getStringExtra("bd")
        if(stringBD == null) {
            stringBD = resources.getString(R.string.baseDeDatos)
        }


        //Se convierte de Strig a JSON
        val bd = JSONObject(stringBD)


        //Obtenemos el semestre
        val nSemestre = alumno.semestre

        val admin = AdminBD()
        var jsonAlumnoHorario = admin.generarHorario(stringBD, nSemestre)

        for(i in 0..jsonAlumnoHorario.length()-1) {
            val jsonDia = jsonAlumnoHorario.getJSONObject(i)

            val linear = LinearLayout(this)
            linear.orientation = LinearLayout.VERTICAL
            linear.gravity = Gravity.CENTER
            linear.layoutParams = ViewGroup.LayoutParams(200,200)
            linear.setBackgroundColor(Color.parseColor("#860027FF"))

            val textViewMateria = TextView(this)
            textViewMateria.setTextColor(Color.BLACK)
            textViewMateria.text = jsonDia.getString("materia")
            linear.addView(textViewMateria)

            val textViewHora = TextView(this)
            textViewHora.setTextColor(Color.BLACK)
            var hora = jsonDia.getString("hora")
            hora = hora.replace("00",":00")
            textViewHora.text = hora
            linear.addView(textViewHora)

            when(jsonDia.getString("dia")) {
                "Lunes" -> linearLunes.addView(linear)
                "Martes" -> linearMartes.addView(linear)
                "Miercoles" -> linearMiercoles.addView(linear)
                "Jueves" -> linearJueves.addView(linear)
                else -> linearViernes.addView(linear)
            }
        }
*/
    }
    }
