package vs.layour.dadm_u1_proyectosge
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.gson.Gson


import vs.layour.dadm_u1_proyectosge.models.dataBase




class  AccederActivity : AppCompatActivity() {

    lateinit var btnAccederAlumno: Button
    lateinit var btnAccederProfesor: Button


    companion object {
        lateinit var bd:dataBase
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_acceder)


        //Obtención de la base de datos completa
        val bdString = resources.getString(R.string.baseDeDatos)
        bd = Gson().fromJson(bdString, dataBase::class.java) as dataBase

        btnAccederAlumno = findViewById<Button>(R.id.btnInicioAlumno)
        btnAccederProfesor = findViewById<Button>(R.id.btnInicioProfesor)


        btnAccederAlumno.setOnClickListener {
            val intent = Intent (this, LogInAlumnoActivity::class.java)
            intent.putExtra("BD", bd)
            startActivity(intent)
        }

        btnAccederProfesor.setOnClickListener {
            val intent = Intent (this, LogInProfesorActivity::class.java)
            intent.putExtra("BD", bd)
            startActivity(intent)
        }
    }


}