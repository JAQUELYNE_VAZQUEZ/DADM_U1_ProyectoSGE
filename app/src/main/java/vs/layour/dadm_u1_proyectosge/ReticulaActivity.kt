package vs.layour.dadm_u1_proyectosge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.Gson
import vs.layour.dadm_u1_proyectosge.adapters.SemestreAdapter
import vs.layour.dadm_u1_proyectosge.databinding.ActivityReticulaBinding
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.dataBase

class ReticulaActivity : AppCompatActivity() {

    private lateinit var binding: ActivityReticulaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReticulaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val alumno = intent.extras?.getSerializable("ALUMNO") as Alumno

        val bdString = resources.getString(R.string.baseDeDatos)
        val bdjson = Gson().fromJson(bdString, dataBase::class.java)
        val reticula = bdjson.reticula

        //Se recorre el semestre
        for (semestre in reticula) {

            val sem = semestre
            //Se recorre la lista de materias del semestre
            for (materia in sem) {
                println(materia.nombre)
            }
        }

        //Se pinta en el layout a segun sea el semestre del JSON
        val lvsemestre1 = binding.lvSemestre1
        lvsemestre1.adapter = SemestreAdapter(this, R.layout.materia, reticula[0])

        val lvsemestre2 = binding.lvSemestre2
        lvsemestre2.adapter = SemestreAdapter(this, R.layout.materia, reticula[1])

        val lvsemestre3 = binding.lvSemestre3
        lvsemestre3.adapter = SemestreAdapter(this, R.layout.materia, reticula[2])

        val lvsemestre4 = binding.lvSemestre4
        lvsemestre4.adapter = SemestreAdapter(this, R.layout.materia, reticula[3])

        val lvsemestre5 = binding.lvSemestre5
        lvsemestre5.adapter = SemestreAdapter(this, R.layout.materia, reticula[4])

        val lvsemestre6 = binding.lvSemestre6
        lvsemestre6.adapter = SemestreAdapter(this, R.layout.materia, reticula[5])

        val lvsemestre7 = binding.lvSemestre7
        lvsemestre7.adapter = SemestreAdapter(this, R.layout.materia, reticula[6])

        val lvsemestre8 = binding.lvSemestre8
        lvsemestre8.adapter = SemestreAdapter(this, R.layout.materia, reticula[7])

        val lvsemestre9 = binding.lvSemestre9
        lvsemestre9.adapter = SemestreAdapter(this, R.layout.materia, reticula[8])


    }
}