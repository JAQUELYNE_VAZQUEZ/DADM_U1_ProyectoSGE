package vs.layour.dadm_u1_proyectosge

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import vs.layour.dadm_u1_proyectosge.databinding.ActivityMenuAlumnoBinding
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.dataBase

class MenuAlumnoActivity : AppCompatActivity() {

    lateinit var stringBD: String
    lateinit var stringAlumno: String

    private lateinit var binding: ActivityMenuAlumnoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuAlumnoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bd = intent.extras?.getSerializable("BD") as dataBase
        val alumno = intent.extras?.getSerializable("ALUMNO") as Alumno

        var btnMisDatos = findViewById<Button>(R.id.btnMenuAlumno_misDatos)
        var btnHorario = findViewById<Button>(R.id.btnMenuAlumno_horario)
        var btnReticula = findViewById<Button>(R.id.btnMenuAlumno_reticula)
        var btnCalificaciones = findViewById<Button>(R.id.btnMenuAlumno_calificaciones)
        var btnEvaluacioDocente = findViewById<Button>(R.id.btnMenuAlumno_evaluacionDocente)



        btnMisDatos.setOnClickListener {
            val intent = Intent(this, MisDatosActivity::class.java)
            intent.putExtra("ALUMNO", alumno)
            startActivity(intent)
        }
        btnHorario.setOnClickListener {
            val intent = Intent(this, HorarioActivity::class.java)
            intent.putExtra("ALUMNO", alumno)
            startActivity(intent)

        }

        btnReticula.setOnClickListener {
            val intent = Intent(this, ReticulaActivity::class.java)
            intent.putExtra("ALUMNO", alumno)
            startActivity(intent)
        }

        btnCalificaciones.setOnClickListener {
            val intent = Intent(this, KardexActivity::class.java)
            startActivity(intent)
        }

        btnEvaluacioDocente.setOnClickListener {

            for (i in AccederActivity.bd.evaluacionesDocentes) {
                if (i.noControlAlumno.equals(alumno.noControl)) {
                    Toast.makeText(this, "Ya se realizo la evaluacion docente", Toast.LENGTH_SHORT)
                        .show()
                    return@setOnClickListener
                }
            }

            val intent = Intent(this, EvaluacionDocenteActivity::class.java)
            intent.putExtra("ALUMNO", alumno)
            intent.putExtra("BD", bd)
            startActivity(intent)

        }

    }

}