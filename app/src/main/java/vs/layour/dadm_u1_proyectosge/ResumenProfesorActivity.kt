package vs.layour.dadm_u1_proyectosge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import vs.layour.dadm_u1_proyectosge.adapters.AlumnosTotalesAdapter
import vs.layour.dadm_u1_proyectosge.adapters.ComentariosDeEvaluacionAdapter
import vs.layour.dadm_u1_proyectosge.databinding.ActivityResumenProfesorBinding
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.Evaluacion
import vs.layour.dadm_u1_proyectosge.models.Profesore


class ResumenProfesorActivity : AppCompatActivity() {

    private lateinit var binding: ActivityResumenProfesorBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResumenProfesorBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bd = AccederActivity.bd
        val alumnos = bd.alumnos
        val usuarioLogueado = intent.extras?.getSerializable("USUARIO_LOGUEADO") as Profesore



        //For anidado, para comprara el nControl del evaluador con el alumno
        val alumnosEvaluadores = ArrayList<Alumno>()
        for (alum in alumnos){
            for (evaluador in bd.evaluacionesDocentes){
                if (alum.noControl.equals(evaluador.noControlAlumno)){
                    alumnosEvaluadores.add(alum)
                }
            }
        }
        val numAlumnosTotales = bd.alumnos.size.toString()
        val numalumnosEvaluadores = bd.evaluacionesDocentes.size.toString()

        println("$numAlumnosTotales  $numalumnosEvaluadores")

        binding.numAlumnosTotales.text = numAlumnosTotales
        binding.numAlumnosEvaluadores.text = numalumnosEvaluadores

        //pinta todos los alumnos del profesor
        binding.lvAlumnosTotales.adapter = AlumnosTotalesAdapter(this, R.layout.total_alumnos_item, alumnos)


        val evaluaciones = ArrayList<Evaluacion>()
        for(i in bd.evaluacionesDocentes){
            if (i.profesor.equals(usuarioLogueado.nombre)){
                evaluaciones.add(i)
            }
        }

        // pintar todos los alumnos que evaluaron al docente
        binding.lvAlumnosEvaluaron.adapter = AlumnosTotalesAdapter(this, R.layout.total_alumnos_item, alumnosEvaluadores)
        //Se pintan numero de los comentarios y comentarios que se recibieron de la evaluación docent
        binding.lvComentarios.adapter = ComentariosDeEvaluacionAdapter(this, R.layout.comentarios_item,evaluaciones)

        binding.numComentarios.setText(evaluaciones.size.toString())


        //-------------------------------GRAFICA -------------------------------



        val listpie = ArrayList<PieEntry>()
        val listColors= ArrayList<Int>()
        


        listpie.add(PieEntry(10f, "Catedra"))
        listColors.add(resources.getColor(R.color.black))

        listpie.add(PieEntry(10f, "evaluacion"))
        listColors.add(resources.getColor(R.color.azulMarino))

        listpie.add(PieEntry(10f, "accesibilidad"))
        listColors.add(resources.getColor(R.color.materiaAcreditada))

        listpie.add(PieEntry(10f, "calidad"))
        listColors.add(resources.getColor(R.color.materiaPosible))

        listpie.add(PieEntry(10f, "profesionalismo"))
        listColors.add(resources.getColor(R.color.cursadaSinAcreditar))



        val piedataset=PieDataSet(listpie, "")
        piedataset.colors = listColors


        binding.piechartView.data = PieData(piedataset)

/*

        //LA OTRA GRAFICA ANDROID XD
        val listpie = ArrayList<Int>()
        val s1 = Segment("Uno", listpie.get(0))
        val s2 = Segment("dos", listpie.get(1))
        val s3 = Segment("tres", listpie.get(2))
        val s4 = Segment("cuatro", listpie.get(3))

        val sf1 = SegmentFormatter(Color.BLUE)
        val sf2 = SegmentFormatter(Color.YELLOW)
        val sf3 = SegmentFormatter(Color.RED)
        val sf4 = SegmentFormatter(Color.GREEN)

        pieChart.addSegment(s1,sf1)


*/
    }
}