package vs.layour.dadm_u1_proyectosge.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Profesore(
    @SerializedName("contrasena")
    val contrasena: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("materia")
    val materia: String,
    @SerializedName("nombre")
    val nombre: String
): Serializable