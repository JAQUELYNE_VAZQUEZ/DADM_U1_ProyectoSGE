package vs.layour.dadm_u1_proyectosge.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Lune(
    @SerializedName("hora")
    val hora: String,
    @SerializedName("materia")
    val materia: String,
    @SerializedName("meteria")
    val meteria: String,
    @SerializedName("profesor")
    val profesor: String
): Serializable