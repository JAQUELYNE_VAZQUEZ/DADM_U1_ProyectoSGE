package vs.layour.dadm_u1_proyectosge.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Alumno(
    @SerializedName("carrera")
    val carrera: String,
    @SerializedName("contrasena")
    val contrasena: String,
    @SerializedName("correoInstitucional")
    val correoInstitucional: String,
    @SerializedName("correoPersonal")
    val correoPersonal: String,
    @SerializedName("curp")
    val curp: String,
    @SerializedName("fNacimiento")
    val fNacimiento: String,
    @SerializedName("grupo")
    val grupo: String,
    @SerializedName("noControl")
    val noControl: String,
    @SerializedName("nombre")
    val nombre: String,
    @SerializedName("semestre")
    val semestre: String,
    @SerializedName("sexo")
    val sexo: String
):Serializable