package vs.layour.dadm_u1_proyectosge.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Evaluacion(
    @SerializedName("id")
    val id: Int,
    @SerializedName("profesor")
    val profesor: String,

    @SerializedName("noControlAlumno")
    val noControlAlumno: String,
    @SerializedName("horaCreacion")
    val horaCreacion: String,
    @SerializedName("comentarios")
    val comentarios: String,

    @SerializedName("r1")
    val catedraRubro: Int,
    @SerializedName("r2")
    val evaluacionRubro: Int,
    @SerializedName("r3")
    val accesibilidadRubro: Int,
    @SerializedName("r4")
    val calidadRubro: Int,
    @SerializedName("r5")
    val profesionalismoRubro: Int
):Serializable