package vs.layour.dadm_u1_proyectosge.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class dataBase(
    @SerializedName("alumnos")
    val alumnos: ArrayList<Alumno>,
    @SerializedName("horario")
    val horario: List<Horario>,
    @SerializedName("profesores")
    val profesores: List<Profesore>,
    @SerializedName("reticula")
    val reticula: List<ArrayList<Materia>>,
    @SerializedName("evaluacionesDocentes")
    val evaluacionesDocentes: ArrayList<Evaluacion>
):Serializable