package vs.layour.dadm_u1_proyectosge

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.gson.Gson
import org.json.JSONObject
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.Profesore
import vs.layour.dadm_u1_proyectosge.models.dataBase

class LogInProfesorActivity : AppCompatActivity() {

    lateinit var editId: EditText
    lateinit var editPass: EditText
    lateinit var btnAcceder: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in_profesor)

            editId = findViewById(R.id.editTextLogInProfesor_Id)
            editPass = findViewById(R.id.editTextLogInProfesor_password)
            btnAcceder = findViewById(R.id.btnLogInProfesor_iniciarsesion)

            val miJson = resources.getString(R.string.baseDeDatos)
            val jsonProfesores = JSONObject(miJson)
            val arrayProfesores = jsonProfesores.getJSONArray("profesores")


            btnAcceder.setOnClickListener {
                val id = editId.text.toString()
                val pass = editPass.text.toString()

                var acceso = false
                for (i in 0..(arrayProfesores.length() - 1)) {
                    print(arrayProfesores[i].toString())

                    val jsonProfesor = arrayProfesores.getJSONObject(i)
                    if (jsonProfesor.getString("id").trim().equals(id.trim())) {
                        acceso = true

                        if (jsonProfesor.getString("contrasena").trim().equals(pass.trim())) {
                            Toast.makeText(this, "Encontrado", Toast.LENGTH_LONG).show()

                            val intent = Intent (this, MenuProfesorActivity::class.java)
                            intent.putExtra("profesor", jsonProfesor.toString())
                            intent.putExtra("bd", miJson)
                            val prof = Gson().fromJson(arrayProfesores[i].toString(), Profesore::class.java)

                            println("----------------")
                            println("$prof")

                            intent.putExtra("USUARIO_LOGUEADO", prof)
                            startActivity(intent)
                            //finish()

                        } else {
                            Toast.makeText(this, "Datos Incorrectos", Toast.LENGTH_LONG).show()
                        }
                    }
                }
                if (!acceso){
                    Toast.makeText(this,"No Existen estos datos", Toast.LENGTH_LONG).show()

                }
            }
        }
    }
