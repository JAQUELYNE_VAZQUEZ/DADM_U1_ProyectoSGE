package vs.layour.dadm_u1_proyectosge.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import vs.layour.dadm_u1_proyectosge.R
import vs.layour.dadm_u1_proyectosge.models.Materia

class SemestreAdapter (val context: Context, val layout: Int, val lista: ArrayList<Materia>) : BaseAdapter() {
    override fun getCount(): Int {
       return lista.size
    }

    override fun getItem(position: Int): Any {
        return lista[position]
    }

    override fun getItemId(position: Int): Long {
        return -1
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val myView = inflater.inflate(layout, null)

        val codigo = myView.findViewById<TextView>(R.id.txt_codigo)
        val nombremat= myView.findViewById<TextView>(R.id.txt_materia)
        val calificacion = myView.findViewById<TextView>(R.id.txt_calificacion)



        codigo.text= lista.get(position).clave
        nombremat.text=lista.get(position).nombre
        calificacion.text=lista.get(position).calificacion


        val pintar = myView.findViewById<LinearLayout>(R.id.reticula)


        if (calificacion.text.toString()=="posible"){
            pintar.setBackgroundColor(context.resources.getColor(R.color.materiaPosible))

        }else if(calificacion.text.toString()== "no posible"){
            pintar.setBackgroundColor(context.resources.getColor(R.color.materiaNoPermitida))

        }else if(calificacion.text.toString()=="cursando"){
            pintar.setBackgroundColor(context.resources.getColor(R.color.materiaCursando))
        }else
            pintar.setBackgroundColor(context.resources.getColor(R.color.materiaAcreditada))

        return myView

    }
}