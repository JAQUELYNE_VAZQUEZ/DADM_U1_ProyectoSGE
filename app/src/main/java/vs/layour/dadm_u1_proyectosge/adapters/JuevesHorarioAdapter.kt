package vs.layour.dadm_u1_proyectosge.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import vs.layour.dadm_u1_proyectosge.R
import vs.layour.dadm_u1_proyectosge.models.Jueve
import vs.layour.dadm_u1_proyectosge.models.Lune
import vs.layour.dadm_u1_proyectosge.models.Marte


class JuevesHorarioAdapter (val context: Context, val layout: Int, val lista: ArrayList<Jueve>):BaseAdapter() {
    override fun getCount(): Int {
       return lista.size
    }

    override fun getItem(position: Int): Any {
        return lista[position]
    }

    override fun getItemId(position: Int): Long {
        return -1
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val myView = inflater.inflate(layout, null)

        val hora = myView.findViewById<TextView>(R.id.txt_horario)
        val materia = myView.findViewById<TextView>(R.id.txt_materia)

        hora.text=lista.get(position).hora
        materia.text=lista.get(position).materia

        return myView


        return myView
    }
}
