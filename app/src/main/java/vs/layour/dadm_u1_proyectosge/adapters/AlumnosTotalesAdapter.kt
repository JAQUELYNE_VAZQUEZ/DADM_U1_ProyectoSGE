package vs.layour.dadm_u1_proyectosge.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.FrameLayout
import android.widget.TextView
import org.w3c.dom.Text
import vs.layour.dadm_u1_proyectosge.R
import vs.layour.dadm_u1_proyectosge.models.Alumno

class AlumnosTotalesAdapter(val context: Context, val layout: Int, val lista:ArrayList<Alumno>) :BaseAdapter(){
    override fun getCount(): Int {
      return lista.size
    }

    override fun getItem(position: Int): Any {
       return lista[position]
    }

    override fun getItemId(position: Int): Long {
        return -1
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val myView = inflater.inflate(layout, null)

        val nombre = myView.findViewById<TextView>(R.id.nombreAlumno)
        val numcontrol = myView.findViewById<TextView>(R.id.noControl)
        val grupo = myView.findViewById<TextView>(R.id.grupo)
        val carrera = myView.findViewById<TextView>(R.id.carrera)

        nombre.text=lista.get(position).nombre
        numcontrol.text=lista.get(position).noControl
        grupo.text=lista.get(position).grupo
        carrera.text=lista.get(position).carrera


        return myView
    }

}