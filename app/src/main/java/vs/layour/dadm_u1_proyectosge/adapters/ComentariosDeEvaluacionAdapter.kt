package vs.layour.dadm_u1_proyectosge.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import vs.layour.dadm_u1_proyectosge.R
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.Evaluacion

class ComentariosDeEvaluacionAdapter (val context: Context, val layout: Int, val lista:ArrayList<Evaluacion>):BaseAdapter(){
    override fun getCount(): Int {
        return lista.size
    }

    override fun getItem(position: Int): Any {
        return lista[position]
    }

    override fun getItemId(position: Int): Long {
        return -1
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val myView = inflater.inflate(layout, null)

        val comentarios = myView.findViewById<TextView>(R.id.txt_comentario)

        comentarios.text = lista.get(position).comentarios

        return myView

    }
}