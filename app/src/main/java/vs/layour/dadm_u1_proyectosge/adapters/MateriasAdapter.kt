package vs.layour.dadm_u1_proyectosge.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import vs.layour.dadm_u1_proyectosge.R
import vs.layour.dadm_u1_proyectosge.models.Profesore


class MateriasAdapter(val context: Context, val layout: Int, val lista:ArrayList<String>):
    BaseAdapter() {
    override fun getCount(): Int {
        return lista.size
    }

    override fun getItem(position: Int): Any {
        return lista[position]
    }

    override fun getItemId(position: Int): Long {
        return -1
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val myView = inflater.inflate(layout, null)

        val nombreMateria = myView.findViewById<TextView>(R.id.txtmaterias)


        val mat = lista[position]
        nombreMateria.text = mat

        return myView
    }
}