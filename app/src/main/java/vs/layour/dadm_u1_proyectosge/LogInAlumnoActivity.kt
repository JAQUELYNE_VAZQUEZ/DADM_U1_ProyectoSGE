package vs.layour.dadm_u1_proyectosge

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import org.json.JSONObject
import vs.layour.dadm_u1_proyectosge.databinding.ActivityLogInAlumnoBinding
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.dataBase


class LogInAlumnoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLogInAlumnoBinding

    lateinit var editNoControl: EditText
    lateinit var editpass: EditText
    lateinit var btnAcceder: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLogInAlumnoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bd = intent.extras?.getSerializable("BD") as dataBase

        editNoControl = findViewById(R.id.editTextLogInAlumno_NControl)
        editpass = findViewById(R.id.editTextLogInAlumno_password)
        btnAcceder = findViewById(R.id.btnLogInAlumno_iniciarsesion)

        val miJson = resources.getString(R.string.baseDeDatos)
        val jsonAlumnos = JSONObject(miJson)
        val arrayAlumnos = jsonAlumnos.getJSONArray("alumnos")

        btnAcceder.setOnClickListener {
            val numcontrol = editNoControl.text.toString()
            val pass = editpass.text.toString()

            var acceso = false
            for (i in 0..(arrayAlumnos.length() - 1)) {


                val jsonEstudiante = arrayAlumnos.getJSONObject(i)
                if (jsonEstudiante.getString("noControl").trim().equals(numcontrol.trim())) {
                    acceso = true

                    if (jsonEstudiante.getString("contrasena").trim().equals(pass.trim())) {
                        Toast.makeText(this, "Encontrado", Toast.LENGTH_LONG).show()

                        // se manda el objeto alumno a la sig activity
                        //println(arrayAlumnos[i].toString())
                        val alumno = Gson().fromJson(arrayAlumnos[i].toString(), Alumno::class.java)


                        val intent = Intent (this, InicioActivity::class.java)

                        intent.putExtra("ALUMNO", alumno)
                        intent.putExtra("BD", bd)

                        startActivity(intent)
                        //finish()

                    } else {
                        Toast.makeText(this, "Datos Incorrectos", Toast.LENGTH_LONG).show()
                    }
                }
            }
            if (!acceso){
                Toast.makeText(this,"No Existen estos datos",Toast.LENGTH_LONG).show()

            }
        }

    }
}